﻿using System;
using System.Data;
using System.Web.UI;

/// <summary>
///     Partial class of the customer contact web page.
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public partial class Contacts : Page
{
    /// <summary>
    ///     The customer list
    /// </summary>
    private CustomerList _customerList;

    /// <summary>
    ///     The selected customer
    /// </summary>
    private Customer _selectedCustomer;

    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ddlCustomers.DataBind();
        }

        this._customerList = CustomerList.GetCustomers();
        this._selectedCustomer = this.GetCustomer();
        this.lblCustomerName.Text = "Information for <span class='changeColor'>" + this._selectedCustomer.Name +
                                    "</span>";
        this.lblPhoneNumber.Text = "<span class='changeColor'>" + this._selectedCustomer.PhoneNumber + "</span>";
        this.lblEmail.Text = "<span class='changeColor'>" + this._selectedCustomer.Email + "</span>";
    }

    /// <summary>
    ///     Handles the SelectedIndexChanged event of the ddlCustomers control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
    {
        this._selectedCustomer = this.GetCustomer();
        this.lblCustomerName.Text = "Information for <span class='changeColor'>" + this._selectedCustomer.Name +
                                    "</span>";

        if (this._customerList.Count == 0)
        {
            return;
        }

        this.phErrorMessage.Visible = false;
        this.lblErrorMessage.Text = "";
    }

    /// <summary>
    ///     Handles the Click event of the btnAddToContacts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnAddToContacts_Click(object sender, EventArgs e)
    {
        if (!this.IsAdded())
        {
            this._customerList.Add(this._selectedCustomer);
        }
    }

    /// <summary>
    ///     Determines whether this instance is added.
    /// </summary>
    /// <returns><c>true</c> if the customer has been added</returns>
    private bool IsAdded()
    {
        const bool isAdded = false;

        if (this._customerList[this._selectedCustomer.Name] == null)
        {
            return isAdded;
        }

        this.phErrorMessage.Visible = true;
        this.lblErrorMessage.Text = "Customer <span class='changeColor'>" + this._selectedCustomer.Name +
                                    "</span> is already added to the list.";

        return !isAdded;
    }

    /// <summary>
    ///     Gets the customer.
    /// </summary>
    /// <returns>The customer</returns>
    private Customer GetCustomer()
    {
        var customerTable = (DataView) this.CustomerDatabase.Select(DataSourceSelectArguments.Empty);

        if (customerTable == null)
        {
            return null;
        }

        customerTable.RowFilter = string.Format("CustomerID = '{0}'", this.ddlCustomers.SelectedValue);
        var row = customerTable[0];
        var customer = new Customer
        {
            CustomerId = row["CustomerID"].ToString(),
            Name = row["Name"].ToString(),
            Email = row["Email"].ToString(),
            PhoneNumber = row["Phone"].ToString(),
            Address = row["Address"].ToString(),
            City = row["City"].ToString(),
            State = row["State"].ToString(),
            ZipCode = row["ZipCode"].ToString()
        };

        return customer;
    }
}