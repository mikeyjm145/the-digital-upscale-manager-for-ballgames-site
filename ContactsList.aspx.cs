﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
///     The Code Behind for the ContactList Page
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public partial class ContactsList : Page
{
    /// <summary>
    ///     The _customer list
    /// </summary>
    private CustomerList _customerList;

    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this._customerList = CustomerList.GetCustomers();

        if (!Page.IsPostBack)
        {
            this.UpdateCustomerListView();
        }

        this.ToggleDisableButtons();
    }

    /// <summary>
    ///     Handles the Click event of the btnRemoveCustomer control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnRemoveCustomer_Click(object sender, EventArgs e)
    {
        var selectedValue = this.lbCustomerListView.SelectedValue;

        if (selectedValue == "")
        {
            return;
        }

        var index = Convert.ToInt32(selectedValue);
        this._customerList.RemoveAt(index);

        this.lbCustomerListView.Items.RemoveAt(this.lbCustomerListView.SelectedIndex);

        this.UpdateCustomerListView();
        this.ToggleDisableButtons();
    }

    /// <summary>
    ///     Handles the Click event of the btnClearList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnClearList_Click(object sender, EventArgs e)
    {
        this._customerList.Clear();
        this.UpdateCustomerListView();
        this.ToggleDisableButtons();
    }

    /// <summary>
    ///     Updates the customer ListView.
    /// </summary>
    private void UpdateCustomerListView()
    {
        this.lbCustomerListView.Items.Clear();
        var customers = this.SortCustomers();
        this.DisplayCustomers(customers);
    }

    /// <summary>
    ///     Sorts the customers.
    /// </summary>
    /// <returns>Sorted list of customers</returns>
    private IEnumerable<ListItem> SortCustomers()
    {
        var customerItems = new List<ListItem>();

        for (var i = 0; i < this._customerList.Count; i++)
        {
            var customer = this._customerList[i];
            var name = customer.Name.Split(' ');
            var customerInfo = name[1] + ", " + name[0] + ": " + customer.PhoneNumber + "; " + customer.Email;
            var customerInfoItem = new ListItem(customerInfo, i.ToString());

            customerItems.Add(customerInfoItem);
        }

        return customerItems.OrderBy(name => name.Text).ToArray();
    }

    /// <summary>
    ///     Displays the customers.
    /// </summary>
    /// <param name="customerListItems">The customer list items.</param>
    private void DisplayCustomers(IEnumerable<ListItem> customerListItems)
    {
        var customers = customerListItems.ToArray();

        foreach (var customer in customers)
        {
            this.lbCustomerListView.Items.Add(customer);
        }
    }

    /// <summary>
    ///     Toggles the disable buttons.
    /// </summary>
    private void ToggleDisableButtons()
    {
        if (this._customerList.Count == 0)
        {
            this.btnClearList.Enabled = false;
            this.btnRemoveCustomer.Enabled = false;
        }
        else
        {
            this.btnClearList.Enabled = true;
            this.btnRemoveCustomer.Enabled = true;
        }
    }
}