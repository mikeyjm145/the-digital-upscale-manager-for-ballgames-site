﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="MaintainSupport.aspx.cs" Inherits="MaintainSupport" %>

<asp:Content ID="Content" ContentPlaceHolderID="Content" Runat="Server">
    <section>
        <section class="tableSection center alignText">
            <section class="center">
                <asp:Label ID="lblSelectMaintenanceStaffMember" runat="server" CssClass="tableTitle" Text="Please select a maintenance person:"></asp:Label>&nbsp;&nbsp;
                <asp:DropDownList ID="ddlSupport" CssClass="ddlCustomersView" runat="server" AutoPostBack="True" Height="30px" Width="300px" DataSourceID="sqldsSupport" DataTextField="Name" DataValueField="SupportID"></asp:DropDownList>
                <asp:SqlDataSource ID="sqldsSupport" runat="server" ConnectionString="<%$ ConnectionStrings:SupportPersonnelConnectionString %>" ProviderName="<%$ ConnectionStrings:SupportPersonnelConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Support] ORDER BY [Name]"></asp:SqlDataSource>
            </section>
        </section>
    </section>
    <br/>
    <section>
        <section class="contentSection center">
            <asp:FormView ID="fvSupportPersonnel" runat="server" DataKeyNames="SupportID" DataSourceID="sqldsSupportDisplay" BackColor="#DEBA84" BorderColor="#DEBA84" BorderWidth="1px" CellPadding="3" CellSpacing="2" GridLines="Both" Width="386px" HeaderText="Customer Service Personnel" HorizontalAlign="Center" OnItemDeleted="fvSupportPersonnel_ItemDeleted" OnItemInserted="fvSupportPersonnel_ItemInserted" OnItemUpdated="fvSupportPersonnel_ItemUpdated">
                    <EditItemTemplate>
                        <table>
                            <tr>
                                <td class="tableColTitleCol">Support ID:</td>
                                <td>
                                    <asp:Label ID="SupportIDLabel" runat="server" Text='<%# Eval("SupportID") %>' Height="22px" Width="203px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tableColTitleCol">Name:</td>
                                <td>
                                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' Height="22px" Width="203px" />
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="NameTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter value into this field.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tableColTitleCol">Email:</td>
                                <td>
                                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' Height="22px" Width="203px" TextMode="Email" ToolTip="Enter an Email here." />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="EmailTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into this field.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="EmailTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a valid value in this field." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tableColTitleCol">Phone:</td>
                                <td>
                                    <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' Height="22px" Width="203px" />
                                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="EmailTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into this field." ToolTip="Enter a Phone Number here.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="PhoneTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into this field." SetFocusOnError="True" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <section class="center alignText" style="width: 350px;">
                                        <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" Text="Update" ToolTip="Update Pesonnel" Width="80px" />
                            &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" Text="Cancel" ToolTip="Cancel" Width="80px" />
                                    </section>
                                </td>
                            </tr>
                        </table>
                    </EditItemTemplate>
                    <EditRowStyle BackColor="WhiteSmoke" Font-Bold="True" ForeColor="DarkRed" BorderColor="DarkRed" />
                    <InsertRowStyle BackColor="WhiteSmoke" Font-Bold="True" ForeColor="DarkRed" BorderColor="DarkRed" />
                    <FooterStyle ForeColor="#8C4510" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <HeaderStyle BackColor="DarkRed" Font-Bold="True" ForeColor="White" Font-Italic="False" Font-Size="X-Large" Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" BorderColor="DarkRed" BorderStyle="Solid" BorderWidth="3px" />
                    <InsertItemTemplate>
                        <table>
                            <tr>
                                <td class="tableColTitleCol">Support ID:</td>
                                <td>
                                    <asp:TextBox ID="SupportIDTextBox" runat="server" Text='<%# Bind("SupportID") %>' Height="22px" Width="203px" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="SupportIDTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a valid value in this field.">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="SupportIDTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid numerical Maintenance Personnel ID" Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">Name:</td>
                                <td class="auto-style6">
                                    <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' Height="22px" Width="203px" />
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="NameTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter value into this field.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tableColTitleCol">Email:</td>
                                <td>
                                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' Height="22px" Width="203px" />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="EmailTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into this field.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="EmailTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a valid value in this field." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tableColTitleCol">Phone:</td>
                                <td>
                                    <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' Height="22px" Width="203px" />
                                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="EmailTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into this field." ToolTip="Enter a Phone Number here.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="PhoneTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into this field." SetFocusOnError="True" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <section class="center alignText" style="width: 350px;">
                                        <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" Text="Insert" ToolTip="Insert New Pesonnel" Width="80px" />
                            &nbsp;<asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" Text="Cancel" ToolTip="Cancel" Width="80px" />
                                    </section>
                                </td>
                            </tr>
                        </table>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td class="tableColTitleCol">Support ID:</td>
                                <td class="auto-style4">
                                    <asp:Label ID="SupportIDLabel" runat="server" Text='<%# Eval("SupportID") %>' Height="22px" Width="203px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style1">Name:</td>
                                <td class="auto-style2">
                                    <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' Height="22px" Width="203px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tableColTitleCol">Email:</td>
                                <td class="auto-style4">
                                    <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' Height="22px" Width="203px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tableColTitleCol">Phone:</td>
                                <td class="auto-style4">
                                    <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' Height="22px" Width="203px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <section class="center alignText" style="width: 350px;">
                                        <asp:Button ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" Text="Edit" ToolTip="Edit Pesonnel" Width="80px" />
                            &nbsp;<asp:Button ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" Text="Delete" ToolTip="Delete Pesonnel" Width="80px" />
                            &nbsp;<asp:Button ID="NewButton" runat="server" CausesValidation="False" CommandName="New" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" Text="New" ToolTip="Add New Pesonnel" Width="80px" />
                                    </section>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <RowStyle BackColor="WhiteSmoke" ForeColor="DarkRed" BorderColor="DarkRed" />
                </asp:FormView>
            <asp:SqlDataSource ID="sqldsSupportDisplay" runat="server" ConnectionString="<%$ ConnectionStrings:SupportPersonnelDisplayConnectionString %>" DeleteCommand="DELETE FROM [Support] WHERE [SupportID] = ?" InsertCommand="INSERT INTO [Support] ([SupportID], [Name], [Email], [Phone]) VALUES (?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:SupportPersonnelDisplayConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Support] WHERE ([SupportID] = ?) ORDER BY [Name], [SupportID]" UpdateCommand="UPDATE [Support] SET [Name] = ?, [Email] = ?, [Phone] = ? WHERE [SupportID] = ?" OnDeleted="sqldsSupportDisplay_Deleted" OnInserted="sqldsSupportDisplay_Inserted" OnUpdated="sqldsSupportDisplay_Updated">
                <DeleteParameters>
                    <asp:Parameter Name="SupportID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="SupportID" Type="Int32" />
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlSupport" Name="SupportID" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="SupportID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:Label ID="lblErrorMessage" runat="server" CssClass="errorMessage" Width="98%"></asp:Label>
            <asp:ValidationSummary ID="vsCustomerServicePersonnel" runat="server" CssClass="errorMessage" HeaderText="Please fix the errors specified below:" />
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="Head">
    <link href="App_Themes/Main.css" rel="stylesheet"/>
    <style type="text/css">
        .auto-style1 {
            width: 80px;
            height: 23px;
        }
        .auto-style2 {
            height: 23px;
            width: 250px;
        }
        .auto-style4 {
            width: 250px;
        }
        .auto-style5 {
            width: 80px;
            height: 26px;
        }
        .auto-style6 {
            height: 26px;
        }
    </style>
</asp:Content>
