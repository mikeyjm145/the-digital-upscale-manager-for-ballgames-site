﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="CustomerMaintenance.aspx.cs" Inherits="CustomerMaintenance2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
    <section>
        <section class="tableSection center">
            <section class="center">
                <asp:Label ID="lblSelectCustomer" runat="server" CssClass="tableTitle" Text="Please select a customer:"></asp:Label> 
            </section>
            
            <asp:GridView ID="gvCustomerMaintenance" runat="server" AllowPaging="True" AutoGenerateColumns="False" ForeColor="#333333" Width="800px" HorizontalAlign="Center" PageSize="6" DataKeyNames="CustomerID" DataSourceID="sqldsCustomerMaintenance" SelectedIndex="0">
                <AlternatingRowStyle BackColor="white" />
                <Columns>
                    <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" SortExpression="CustomerID" InsertVisible="False" ReadOnly="True" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                    <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                    <asp:CommandField ShowSelectButton="True" />
                </Columns>
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="darkred" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="WhiteSmoke" ForeColor="DarkRed" HorizontalAlign="Center" Font-Bold="True" VerticalAlign="Middle" />
                <RowStyle BackColor="antiquewhite" HorizontalAlign="Center" VerticalAlign="Middle" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <asp:SqlDataSource ID="sqldsCustomerMaintenance" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerMaintenanceConnectionString %>" ProviderName="<%$ ConnectionStrings:CustomerMaintenanceConnectionString.ProviderName %>" SelectCommand="SELECT CustomerID, Name, City, State FROM Customer ORDER BY Name"></asp:SqlDataSource>
        </section>
    </section>
    <br/>
    <section>
        <section class="contentSection center">
            <asp:DetailsView ID="dvCustomerMaintenance" runat="server" CellPadding="3" CellSpacing="2" Width="300px" AutoGenerateRows="False" DataSourceID="sqldsCustomerMaintenanceAddInsertDelete" GridLines="None" HeaderText="Customer Information" HorizontalAlign="Center" OnItemDeleted="dvCustomerMaintenance_ItemDeleted" OnItemInserted="dvCustomerMaintenance_ItemInserted" OnItemUpdated="dvCustomerMaintenance_ItemUpdated" DataKeyNames="CustomerID">
                <AlternatingRowStyle BackColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                <CommandRowStyle BackColor="WhiteSmoke" Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                <FieldHeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="70px" />
                <Fields>
                    <asp:TemplateField HeaderText="CustomerID" SortExpression="CustomerID">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("CustomerID") %>'></asp:Label>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerID" runat="server" Text='<%# Bind("CustomerID") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerName" runat="server" ControlToValidate="tbCustomerID" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("CustomerID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" SortExpression="Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbCustomerName" runat="server" Text='<%# Bind("Name") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerName" runat="server" ControlToValidate="tbCustomerName" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerName1" runat="server" Text='<%# Bind("Name") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerName1" runat="server" ControlToValidate="tbCustomerName1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address" SortExpression="Address">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbCustomerAddress" runat="server" Text='<%# Bind("Address") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerAddress" runat="server" ControlToValidate="tbCustomerAddress" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerAddress1" runat="server" Text='<%# Bind("Address") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerAddress1" runat="server" ControlToValidate="tbCustomerAddress1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City" SortExpression="City">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbCustomerCity" runat="server" Text='<%# Bind("City") %>' Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCutomerCity" runat="server" ControlToValidate="tbCustomerCity" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerCity1" runat="server" Text='<%# Bind("City") %>' Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerCity1" runat="server" ControlToValidate="tbCustomerCity1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="State" SortExpression="State">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbCustomerState" runat="server" Text='<%# Bind("State") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerState" runat="server" ControlToValidate="tbCustomerState" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerState1" runat="server" Text='<%# Bind("State") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerState1" runat="server" ControlToValidate="tbCustomerState1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("State") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ZipCode" SortExpression="ZipCode">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbCustomerZipCode" runat="server" Text='<%# Bind("ZipCode") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerZipCode" runat="server" ControlToValidate="tbCustomerZipCode" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerZipCode" runat="server" ControlToValidate="tbCustomerZipCode" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter zip code with valid format." ValidationExpression="\d{5}(-\d{4})?" ValidationGroup="insert_delete_update" Text="*">*</asp:RegularExpressionValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerZipCode1" runat="server" Text='<%# Bind("ZipCode") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerZipCode1" runat="server" ControlToValidate="tbCustomerZipCode1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerZipCode1" runat="server" ControlToValidate="tbCustomerZipCode1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter zip code with valid format." ValidationExpression="\d{5}(-\d{4})?" ValidationGroup="insert_delete_update" Text="*">*</asp:RegularExpressionValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ZipCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbCustomerPhone" runat="server" Text='<%# Bind("Phone") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerPhone" runat="server" ControlToValidate="tbCustomerPhone" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerPhone" runat="server" ControlToValidate="tbCustomerPhone" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid phone number." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="insert_delete_update" Text="*">*</asp:RegularExpressionValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerPhone1" runat="server" Text='<%# Bind("Phone") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerPhone1" runat="server" ControlToValidate="tbCustomerPhone1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerPhone1" runat="server" ControlToValidate="tbCustomerPhone1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid phone number." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="insert_delete_update" Text="*">*</asp:RegularExpressionValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" SortExpression="Email">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbCustomerEmail" runat="server" Text='<%# Bind("Email") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerEmail" runat="server" ControlToValidate="tbCustomerEmail" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update" Text="*">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerEmail" runat="server" ControlToValidate="tbCustomerEmail" CssClass="errorMessage" Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="insert_delete_update">*</asp:RegularExpressionValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="tbCustomerEmail1" runat="server" Text='<%# Bind("Email") %>' ValidationGroup="insert_delete_update" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCustomerEmail1" runat="server" ControlToValidate="tbCustomerEmail1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert_delete_update">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerEmail1" runat="server" ControlToValidate="tbCustomerEmail1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="insert_delete_update" Text="*">*</asp:RegularExpressionValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                </Fields>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" HorizontalAlign="Center" VerticalAlign="Middle" />
                <HeaderStyle BackColor="DarkRed" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                <InsertRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Font-Bold="False" />
                <RowStyle BackColor="AntiqueWhite" ForeColor="#8C4510" HorizontalAlign="Left" VerticalAlign="Middle" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="sqldsCustomerMaintenanceAddInsertDelete" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerMaintenanceFullDetailsConnectionString %>" DeleteCommand="DELETE FROM [Customer] WHERE [CustomerID] = ?" InsertCommand="INSERT INTO [Customer] ([CustomerID], [Name], [Address], [City], [State], [ZipCode], [Phone], [Email]) VALUES (?, ?, ?, ?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:CustomerMaintenanceFullDetailsConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Customer] WHERE ([CustomerID] = ?)" UpdateCommand="UPDATE [Customer] SET [Name] = ?, [Address] = ?, [City] = ?, [State] = ?, [ZipCode] = ?, [Phone] = ?, [Email] = ? WHERE [CustomerID] = ?">
                <DeleteParameters>
                    <asp:Parameter Name="CustomerID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="CustomerID" Type="Int32" />
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="City" Type="String" />
                    <asp:Parameter Name="State" Type="String" />
                    <asp:Parameter Name="ZipCode" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="gvCustomerMaintenance" Name="CustomerID" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="City" Type="String" />
                    <asp:Parameter Name="State" Type="String" />
                    <asp:Parameter Name="ZipCode" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="CustomerID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:Label ID="lblErrorMessage" runat="server" CssClass="errorMessage" Width="97%"></asp:Label>
            <asp:ValidationSummary ID="vsCustomerMaintenance" runat="server" CssClass="errorMessage" DisplayMode="List" HeaderText="Please correct these errors:" ValidationGroup="insert_delete_update" />
        </section>
    </section>
</asp:Content>

