﻿<%@ Page Title="DUMB Customer Database" MasterPageFile="~/DUMBMasterPage.master" Language="C#" AutoEventWireup="true" CodeFile="FeedbackComplete.aspx.cs" Inherits="FeedbackComplete" %>

<asp:Content id="PageContent" runat="server" ContentPlaceHolderID="Content">
        <section id="customer_selection_area">
            <section class="button_section border_for_section">
                <section class="button_section">
                    <asp:Label ID="lblConfirmationTitle" runat="server" Text="Label" CssClass="view_customer_labels"></asp:Label>
                    <br/>
                    <asp:Label ID="lblConfirmationText" runat="server" Text="Label" CssClass="confirmation_text_style"></asp:Label>
                </section>
                <section class="button_section">
                    <asp:Button ID="btnBackToHome" runat="server" Text="Back to Home" CausesValidation="False" CssClass="form_buttons" PostBackUrl="~/default.aspx" UseSubmitBehavior="False" TabIndex="1"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnBackToFeedback" runat="server" Text="Back to Feedback" CausesValidation="False" CssClass="form_buttons" PostBackUrl="~/CustomerFeedback.aspx" ToolTip="Returns back to a new version of the previous page" UseSubmitBehavior="False" TabIndex="2"/>
                </section>
            </section>
        </section>
</asp:Content>