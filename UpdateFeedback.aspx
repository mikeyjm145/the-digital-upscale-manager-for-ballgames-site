﻿<%@ Page Title="Update Feedback" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="UpdateFeedback.aspx.cs" Inherits="UpdateFeedback" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
    <section>
        <section class="tableSection center alignText">
            <section class="center">
                <asp:Label ID="lblSelectMaintenanceStaffMember" runat="server" CssClass="tableTitle" Text="Please select a customer:"></asp:Label>&nbsp;&nbsp;
                <asp:DropDownList ID="ddlCustomersWithFeedback" CssClass="ddlCustomersView" runat="server" AutoPostBack="True" Height="30px" Width="300px" DataSourceID="odsCustomers" DataTextField="Name" DataValueField="CustomerId"></asp:DropDownList>
                <asp:ObjectDataSource ID="odsCustomers" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetSupportStaffMembers" TypeName="CustomerDb"></asp:ObjectDataSource>
                <asp:GridView ID="gvFeedback" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataSourceID="odsFeedback" GridLines="Horizontal" Width="798px" HorizontalAlign="Center" OnRowCancelingEdit="gvFeedback_RowCancelingEdit" OnRowUpdated="gvFeedback_RowUpdated" DataKeyNames="FeedbackId">
                    <AlternatingRowStyle BackColor="white" />
                    <Columns>
                        <asp:BoundField DataField="FeedbackId" HeaderText="" SortExpression="FeedbackId" ReadOnly="True" Visible="false" />
                        <asp:BoundField DataField="SoftwareId" HeaderText="SoftwareId" SortExpression="SoftwareId" ReadOnly="True" />
                        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
                        <asp:BoundField DataField="SupportId" HeaderText="SupportId" SortExpression="SupportId" ReadOnly="True" />
                        <asp:BoundField DataField="DateOpen" HeaderText="DateOpen" SortExpression="DateOpen" ReadOnly="True" />
                        <asp:TemplateField HeaderText="DateClosed" SortExpression="DateClosed">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbDateClosed" runat="server" Text='<%# Bind("DateClosed") %>' Width="170px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDateClosed" runat="server" ControlToValidate="tbDateClosed" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into this field.">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbDateClosed" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a valid date." ValidationExpression="^([1-9]|1[012])[- /.]([1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d\s(1[0-2]|[1-9]):[0-5][0-9]:[0-5][0-9]\s(AM|am|PM|pm)$">*</asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDateClosed" runat="server" Text='<%# Bind("DateClosed") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" SortExpression="Description">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbDescription" runat="server" Text='<%# Bind("Description") %>' Height="70px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value in this field." ControlToValidate="tbDescription">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" ButtonType="Button" />
                        <asp:BoundField DataField="CustomerId" HeaderText="CustomerId" ReadOnly="True" ShowHeader="False" SortExpression="CustomerId" Visible="False" />
                        <asp:BoundField DataField="FeedbackId" HeaderText="FeedbackId" ReadOnly="True" SortExpression="FeedbackId" Visible="False" />
                    </Columns>
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="darkred" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="WhiteSmoke" ForeColor="DarkRed" HorizontalAlign="Center" Font-Bold="True" VerticalAlign="Middle" />
                    <RowStyle BackColor="antiquewhite" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                <asp:ObjectDataSource ID="odsFeedback" runat="server" OldValuesParameterFormatString="original{0}" SelectMethod="GetCustomerFeedback" TypeName="FeedbackDb" UpdateMethod="UpdateFeedback" OnUpdated="odsFeedback_Updated">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlCustomersWithFeedback" Name="customerId" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="originalFeedbackId" Type="String" />
                        <asp:Parameter Name="dateClosed" Type="String" />
                        <asp:Parameter Name="description" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:Label ID="lblError" runat="server" CssClass="errorMessage"></asp:Label>
                <asp:ValidationSummary ID="vsUpdateFeedback" runat="server" CssClass="errorMessage" DisplayMode="List" HeaderText="Please correct these errors:" />
            </section>
        </section>
    </section>
</asp:Content>