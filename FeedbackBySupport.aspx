﻿<%@ Page Title="Feedback By Support" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="FeedbackBySupport.aspx.cs" Inherits="FeedbackBySupport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
    <section>
        <section class="tableSection center alignText">
            <section class="center">
                <asp:Label ID="lblSelectMaintenanceStaffMember" runat="server" CssClass="tableTitle" Text="Please select a maintenance staff member:"></asp:Label>&nbsp;&nbsp;
                <asp:DropDownList ID="ddlSupport" CssClass="ddlCustomersView" runat="server" AutoPostBack="True" Height="30px" Width="300px" DataSourceID="odsSupportStaff" DataTextField="Name" DataValueField="SupportId"></asp:DropDownList>
                <asp:ObjectDataSource ID="odsSupportStaff" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetSupportStaffMembers" TypeName="SupportDb"></asp:ObjectDataSource>
            </section>
        </section>
    </section>
    <br/>
    <section>
        <section class="contentSection center">
            <section class="tableSection center">
                <asp:GridView ID="gvCustomerMaintenance" runat="server" AutoGenerateColumns="False" ForeColor="#333333" Width="800px" HorizontalAlign="Center" DataSourceID="odsIncidents">
                    <AlternatingRowStyle BackColor="white" />
                    <Columns>
                        <asp:BoundField DataField="Software" HeaderText="Software/Incident" SortExpression="Software" />
                        <asp:BoundField DataField="Customer" HeaderText="Customer Name" SortExpression="Customer" />
                        <asp:BoundField DataField="DateOpened" HeaderText="Date Open" SortExpression="DateOpened" />
                    </Columns>
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="darkred" Font-Bold="True" ForeColor="White" Height="30px" />
                    <PagerStyle BackColor="WhiteSmoke" ForeColor="DarkRed" HorizontalAlign="Center" Font-Bold="True" VerticalAlign="Middle" />
                    <RowStyle BackColor="antiquewhite" HorizontalAlign="Center" VerticalAlign="Middle" Height="29px" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
            </section>
            <asp:ObjectDataSource ID="odsIncidents" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetOpenFeedbackIncidents" TypeName="FeedbackDb">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlSupport" Name="supportStaffId" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="Head">
    <link href="App_Themes/Main.css" rel="stylesheet"/>
    </asp:Content>

