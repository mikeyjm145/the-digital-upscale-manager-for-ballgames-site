﻿<%@ Page Title="Customer Feedback" MasterPageFile="~/DUMBMasterPage.master" Language="C#" AutoEventWireup="true" CodeFile="CustomerFeedback.aspx.cs" Inherits="CustomerFeedback" %>

<asp:Content id="PageContent" runat="server" ContentPlaceHolderID="Content">
        <section id="customer_selection_area">

            <section class="button_section">
                <asp:Panel Id="pCustomerID" runat="server" DefaultButton="btnCustomerIDCheck" ValidateRequestMode="Disabled">
                    <asp:Label ID="lblEnterCustomerID" runat="server" CssClass="lblCustomerInfo" Text="Enter Customer ID:"></asp:Label>
                    &nbsp;&nbsp;
                    <asp:TextBox ID="tbCustomerId" CssClass="ddlCustomersView" runat="server" TabIndex="1" ToolTip="Enter a customer's ID to see their feedback" Width="200px" ValidationGroup="customer_search_validation" OnTextChanged="tbCustomerId_TextChanged"></asp:TextBox>
                    &nbsp;&nbsp;
                    <asp:Button ID="btnCustomerIDCheck" CssClass="form_buttons" runat="server" TabIndex="2" Text="Check Customer ID" UseSubmitBehavior="False" OnClick="btnCustomerIDCheck_Click" ToolTip="Checks whether the customer id exists" ValidationGroup="customer_search_validation" ViewStateMode="Enabled" CausesValidation="False"/>
                </asp:Panel>
            </section>

            <asp:Panel Id="pCustomerServiceRating" runat="server" DefaultButton="btnSubmit">
                <section class="button_section">
                    <asp:SqlDataSource ID="sdsCustomerFeedbackSource" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerSupportConnectionString %>" ProviderName="<%$ ConnectionStrings:CustomerSupportConnectionString.ProviderName %>" SelectCommand="SELECT [FeedbackID], [CustomerID], [SoftwareID], [SupportID], [DateOpened], [DateClosed], [Title], [Description] FROM [Feedback]"></asp:SqlDataSource>
                    <asp:RequiredFieldValidator ID="rfvCustomerID" runat="server" ControlToValidate="tbCustomerId" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a value into the Customer ID field" ValidationGroup="customer_search_validation" SetFocusOnError="True" EnableClientScript="False">Please enter a value into the Customer ID field</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvCustomerID" runat="server" Display="Dynamic" ErrorMessage="Please enter a valid customer id" ControlToValidate="tbCustomerId" CssClass="errorMessage" Operator="DataTypeCheck" Type="Integer" ValidationGroup="customer_search_validation" SetFocusOnError="True" EnableClientScript="False">Please enter a valid customer id</asp:CompareValidator>
                </section>

                <section class="button_section">
                    <asp:Label ID="lblCustomerFeedback" runat="server" CssClass="lblCustomerInfo" Text="Customer Feedback:"></asp:Label>
                    <asp:ListBox ID="lbCustomerFeedback" runat="server" CssClass="lbCustomerListView" Height="100px" Enabled="False" ValidationGroup="submit_validation" TabIndex="3" CausesValidation="True"></asp:ListBox>
                    
                    <br/>

                    <asp:RequiredFieldValidator ID="rfvCustomerFeedbackSelection" runat="server" ControlToValidate="lbCustomerFeedback" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please select a feedback from the list" ValidationGroup="submit_validation">Please select a feedback from the list</asp:RequiredFieldValidator>
                </section>

                <section class="button_section">
                    <label class="float_left_feedback_label view_customer_sublabels">
                        Service Time<asp:RequiredFieldValidator ID="rfvServiceTime" runat="server" CssClass="errorMessage" ErrorMessage="*" ControlToValidate="rbgSatisfactoryRatingServiceTime" Display="Dynamic" ValidationGroup="submit_validation">*</asp:RequiredFieldValidator>
                    </label>

                    <section class="radio_button_section_satifactory">
                        <asp:RadioButtonList ID="rbgSatisfactoryRatingServiceTime" CssClass="satisfaction_radio_buttons" runat="server" RepeatDirection="Horizontal" Enabled="False" Font-Size="Large" ValidationGroup="submit_validation" TabIndex="4">
                            <asp:ListItem Value="1">Satisfied</asp:ListItem>
                            <asp:ListItem Value="2">Neither Satisfied or Disatified</asp:ListItem>
                            <asp:ListItem Value="3">Disatisfied</asp:ListItem>
                        </asp:RadioButtonList>
                    </section>

                    <br/>

                    <br/>

                    <label class="float_left_feedback_label view_customer_sublabels">
                        Technical Efficiency<asp:RequiredFieldValidator ID="rfvTechnicalEfficiency" runat="server" ControlToValidate="rbgSatisfactoryRatingTechnicalEfficiency" CssClass="errorMessage" Display="Dynamic" ErrorMessage="*" ValidationGroup="submit_validation">*</asp:RequiredFieldValidator>
                    </label>

                    <section class="radio_button_section_satifactory">
                        <asp:RadioButtonList ID="rbgSatisfactoryRatingTechnicalEfficiency" CssClass="satisfaction_radio_buttons" runat="server" RepeatDirection="Horizontal" Enabled="False" Font-Size="Large" ValidationGroup="submit_validation" TabIndex="5">
                            <asp:ListItem Value="1">Satisfied</asp:ListItem>
                            <asp:ListItem Value="2">Neither Satisfied or Disatified</asp:ListItem>
                            <asp:ListItem Value="3">Disatisfied</asp:ListItem>
                        </asp:RadioButtonList>
                    </section>

                    <br/>

                    <label class="float_left_feedback_label view_customer_sublabels">
                        Problem Resolution<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rbgSatisfactoryRatingProblemResolution" CssClass="errorMessage" Display="Dynamic" ErrorMessage="*" ValidationGroup="submit_validation">*</asp:RequiredFieldValidator>
                        &nbsp;
                    </label>
                    
                    <section class="radio_button_section_satifactory">
                        <asp:RadioButtonList ID="rbgSatisfactoryRatingProblemResolution" CssClass="satisfaction_radio_buttons" runat="server" RepeatDirection="Horizontal" Enabled="False" Font-Size="Large" ValidationGroup="submit_validation" TabIndex="6">
                            <asp:ListItem Value="1">Satisfied</asp:ListItem>
                            <asp:ListItem Value="2">Neither Satisfied or Disatified</asp:ListItem>
                            <asp:ListItem Value="3">Disatisfied</asp:ListItem>
                        </asp:RadioButtonList>
                    </section>

                    <section class="button_section">
                        <label class="lblCustomerInfo view_customer_sublabels">Additional Comments:</label>
                        <br/>
                        <asp:TextBox ID="tbAdditionalComments" runat="server" CssClass="lbCustomerListView" TextMode="MultiLine" Height="100px" TabIndex="7"></asp:TextBox>
                    </section>

                    <section class="button_section">
                        <label class="float_left_feedback_label view_customer_sublabels">
                            Contact By:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="rbgContactBy" CssClass="errorMessage" Display="Dynamic" ErrorMessage="*" ValidationGroup="submit_validation">*</asp:RequiredFieldValidator>
                        </label>

                        <section class="radio_button_section_contact">
                            <asp:RadioButtonList ID="rbgContactBy" CssClass="satisfaction_radio_buttons" runat="server" RepeatDirection="Horizontal" Enabled="False" Font-Size="Large" ValidationGroup="submit_validation" ToolTip="Submit the information" TabIndex="8">
                                <asp:ListItem Value="0">Email</asp:ListItem>
                                <asp:ListItem Value="1">Phone</asp:ListItem>
                            </asp:RadioButtonList>
                        </section>

                        <br/>

                        <asp:CheckBox ID="cbContact" runat="server" CssClass="view_customer_sublabels" Font-Size="Large" Text="Contact Me" ValidationGroup="submit_validation" TabIndex="9"/>
                    </section>

                    <section class="button_section">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="form_buttons" Width="200px" Enabled="False" OnClick="btnSubmit_Click" TabIndex="10" ValidationGroup="submit_validation" UseSubmitBehavior="False"/>
                    </section>
                </section>
            </asp:Panel>
        </section>
</asp:Content>