﻿using System;
using System.Web.UI;

/// <summary>
/// This is the code-behind for the MaintainSupport page
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class MaintainSupport : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the Deleted event of the sqldsSupportDisplay control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.SqlDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void sqldsSupportDisplay_Deleted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
    {
        this.lblErrorMessage.Text = "";
        if (e.Exception == null)
        {
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.fvSupportPersonnel.DataBind();
            this.ddlSupport.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
        
    }

    /// <summary>
    /// Handles the ItemDeleted event of the fvSupportPersonnel control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.FormViewDeletedEventArgs"/> instance containing the event data.</param>
    protected void fvSupportPersonnel_ItemDeleted(object sender, System.Web.UI.WebControls.FormViewDeletedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.fvSupportPersonnel.DataBind();
            this.ddlSupport.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the Updated event of the sqldsSupportDisplay control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.SqlDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void sqldsSupportDisplay_Updated(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
            }

            this.fvSupportPersonnel.DataBind();
            this.ddlSupport.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the ItemUpdated event of the fvSupportPersonnel control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.FormViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void fvSupportPersonnel_ItemUpdated(object sender, System.Web.UI.WebControls.FormViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected";
            }

            this.fvSupportPersonnel.DataBind();
            this.ddlSupport.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
        e.KeepInEditMode = true;
    }

    /// <summary>
    /// Handles the Inserted event of the sqldsSupportDisplay control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.SqlDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void sqldsSupportDisplay_Inserted(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
    {
        const string exception = "The changes you requested to the table were not successful because " +
                                 "they would create duplicate values in the index, primary key, or " +
                                 "relationship. Change the data in the field or fields that contain " +
                                 "duplicate data, remove the index, or redefine the index to permit " +
                                 "duplicate entries and try again.";

        if (e.Exception == null || e.Exception.Message.Equals(exception))
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.fvSupportPersonnel.DataBind();
            this.ddlSupport.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the ItemInserted event of the fvSupportPersonnel control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.FormViewInsertedEventArgs"/> instance containing the event data.</param>
    protected void fvSupportPersonnel_ItemInserted(object sender, System.Web.UI.WebControls.FormViewInsertedEventArgs e)
    {
        const string exception = "The changes you requested to the table were not successful because " +
                                 "they would create duplicate values in the index, primary key, or " +
                                 "relationship. Change the data in the field or fields that contain " +
                                 "duplicate data, remove the index, or redefine the index to permit " +
                                 "duplicate entries and try again.";
        if (e.Exception == null || e.Exception.Message.Equals(exception))
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.fvSupportPersonnel.DataBind();
            this.ddlSupport.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.KeepInInsertMode = true;
        e.ExceptionHandled = true;
    }
}