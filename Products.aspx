﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Products" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
        <section>
            <section class="tableSection center">
                <asp:GridView ID="gvSoftwareProducts" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="SoftwareID" DataSourceID="sqldsProducts" ForeColor="#333333" Width="800px" Height="169px" HorizontalAlign="Center" OnRowUpdated="gvSoftwareProducts_RowUpdated">
                    <AlternatingRowStyle BackColor="white" />
                    <Columns>
                        <asp:TemplateField HeaderText="SoftwareID" SortExpression="SoftwareID">
                            <EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("SoftwareID") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("SoftwareID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" SortExpression="Name">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbSoftwareName" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvSoftwareName" runat="server" ControlToValidate="tbSoftwareName" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a valid value here. &lt;br/&gt;Example: John Doe" ValidationGroup="edit" Text="*">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Version" SortExpression="Version">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbSoftwareVersion" runat="server" Text='<%# Bind("Version") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvSoftwareVersion" runat="server" ControlToValidate="tbSoftwareVersion" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter a valid value here.&lt;br/&gt;Example: 1.0" ValidationGroup="edit" Text="*">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvSoftwareVersion" runat="server" ControlToValidate="tbSoftwareVersion" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." OnServerValidate="cvSoftwareVersion_ServerValidate" ValidationGroup="edit" Text="*">*</asp:CustomValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Version") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ReleaseDate" SortExpression="ReleaseDate">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbSoftwareReleaseDate" runat="server" Text='<%# Bind("ReleaseDate") %>' TextMode="Date"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvSoftwareReleaseDate" runat="server" ControlToValidate="tbSoftwareReleaseDate" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid date." ValidationGroup="edit" Text="*">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revSoftwareReleaseDate" runat="server" ControlToValidate="tbSoftwareReleaseDate" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid date in the format: mm/dd/yyyy HH:mm AM/PM" ValidationExpression="^([1-9]|1[012])[- /.]([1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d\s(1[0-2]|[1-9]):[0-5][0-9]:[0-5][0-9]\s(AM|am|PM|pm)$" ValidationGroup="edit" Text="*">*</asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" ValidationGroup="edit" ShowDeleteButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#FFFF99" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="darkred" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="antiquewhite" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="errorMessage" Width="97%"></asp:Label>
                <asp:SqlDataSource ID="sqldsProducts" runat="server" ConnectionString="<%$ ConnectionStrings:ProductUpdateSupportConnectionString %>" DeleteCommand="DELETE FROM [Software] WHERE [SoftwareID] = ?" InsertCommand="INSERT INTO [Software] ([SoftwareID], [Name], [Version], [ReleaseDate]) VALUES (?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:ProductUpdateSupportConnectionString.ProviderName %>" SelectCommand="SELECT [SoftwareID], [Name], [Version], [ReleaseDate] FROM [Software] ORDER BY [SoftwareID]" UpdateCommand="UPDATE [Software] SET [Name] = ?, [Version] = ?, [ReleaseDate] = ? WHERE [SoftwareID] = ?">
                    <DeleteParameters>
                        <asp:Parameter Name="SoftwareID" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="SoftwareID" Type="String" />
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Version" Type="Decimal" />
                        <asp:Parameter Name="ReleaseDate" Type="DateTime" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Version" Type="Decimal" />
                        <asp:Parameter Name="ReleaseDate" Type="DateTime" />
                        <asp:Parameter Name="SoftwareID" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:ValidationSummary ID="vsSoftwareProductEdit" runat="server" CssClass="errorMessage" HeaderText="Please correct the following errors:" ValidationGroup="edit" DisplayMode="List" Width="780px" />
            </section>
        </section>
    <br/>
        <section>
            <section class="contentSection center">
                <asp:FormView ID="fvInsertSoftware" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="SoftwareID" DataSourceID="sqldsInsertSoftware" DefaultMode="Insert" GridLines="Both" HeaderText="Insert Software Here" HorizontalAlign="Center" Width="400px" OnItemInserted="fvInsertSoftware_ItemInserted">
                    <EditItemTemplate>
                        SoftwareID:
                        <asp:TextBox ID="SoftwareIDTextBox" runat="server" Text='<%# Bind("SoftwareID") %>' />
                        <br />
                        Name:
                        <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                        <br />
                        Version:
                        <asp:TextBox ID="VersionTextBox" runat="server" Text='<%# Bind("Version") %>' />
                        <br />
                        ReleaseDate:
                        <asp:TextBox ID="ReleaseDateTextBox" runat="server" Text='<%# Bind("ReleaseDate") %>' />
                        <br />
                        <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                        &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                    </EditItemTemplate>
                    <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="DarkRed" Font-Bold="True" ForeColor="White" Font-Italic="False" Font-Size="X-Large" Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <InsertItemTemplate>
                        <table>
                            <tr>
                                <td class="tableCol">SoftwareID:</td>
                                <td><asp:TextBox ID="SoftwareIDLabel1" runat="server" Text='<%# Bind("SoftwareID") %>' />
                                    <asp:RequiredFieldValidator ID="rfvSoftwareID" runat="server" ControlToValidate="SoftwareIDLabel1" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert" Text="*">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Name:</td>
                                <td><asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' />
                                    <asp:RequiredFieldValidator ID="rfvSoftwareName" runat="server" ControlToValidate="NameTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert" Text="*">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Version:</td>
                                <td><asp:TextBox ID="VersionTextBox" runat="server" Text='<%# Bind("Version") %>' />
                                    <asp:RequiredFieldValidator ID="rfvSoftwareVersion" runat="server" ControlToValidate="VersionTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert" Text="*">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvSoftwareVersion" runat="server" ControlToValidate="VersionTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid number in this field." Operator="DataTypeCheck" Type="Double" ValidationGroup="insert" Text="*">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>ReleaseDate:</td>
                                <td><asp:TextBox ID="ReleaseDateTextBox" runat="server" Text='<%# Bind("ReleaseDate") %>' />
                                    <asp:RequiredFieldValidator ID="rfvSoftwareReleaseDate" runat="server" ControlToValidate="ReleaseDateTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid value here." ValidationGroup="insert" Text="*">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revSoftwareReleaseDate" runat="server" ControlToValidate="ReleaseDateTextBox" CssClass="errorMessage" Display="Dynamic" ErrorMessage="Please enter valid date value here: mm/dd/yyyy HH:MM:SS AM/PM" ValidationExpression="^([1-9]|1[012])[- /.]([1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d\s(1[0-2]|[1-9]):[0-5][0-9]:[0-5][0-9]\s(AM|am|PM|pm)$" ValidationGroup="insert" Text="*">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <section class="center alignText" style="width: 110px;">
                                        <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Add" ValidationGroup="insert" BackColor="DarkRed" ForeColor="White" Font-Bold="True" Font-Overline="False" Font-Size="Small" ToolTip="Add a software." Width="50px" />
                                        &nbsp;<asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Clear" BackColor="DarkRed" Font-Size="Small" Font-Bold="True" ForeColor="White" Width="50px" />
                                    </section>
                                </td>
                            </tr>
                        </table>
                    </InsertItemTemplate>
                    <InsertRowStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="200px" />
                    <ItemTemplate>
                        SoftwareID:
                        <asp:Label ID="SoftwareIDLabel" runat="server" Text='<%# Eval("SoftwareID") %>' />
                        <br />
                        Name:
                        <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>' />
                        <br />
                        Version:
                        <asp:Label ID="VersionLabel" runat="server" Text='<%# Bind("Version") %>' />
                        <br />
                        ReleaseDate:
                        <asp:Label ID="ReleaseDateLabel" runat="server" Text='<%# Bind("ReleaseDate") %>' />
                        <br />
                    </ItemTemplate>
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="DarkRed" HorizontalAlign="Left" VerticalAlign="Middle" Width="200px" />
                </asp:FormView>
                
                <asp:ValidationSummary ID="vsSoftwareInsert" runat="server" CssClass="errorMessage" DisplayMode="List" HeaderText="Please correct the following errors:" ValidationGroup="insert" />
                <asp:SqlDataSource ID="sqldsInsertSoftware" runat="server" ConnectionString="<%$ ConnectionStrings:ProductUpdateSupportConnectionString %>" ProviderName="<%$ ConnectionStrings:ProductUpdateSupportConnectionString.ProviderName %>" SelectCommand="SELECT [SoftwareID], [Name], [Version], [ReleaseDate] FROM [Software]" DeleteCommand="DELETE FROM [Software] WHERE [SoftwareID] = ?" InsertCommand="INSERT INTO [Software] ([SoftwareID], [Name], [Version], [ReleaseDate]) VALUES (?, ?, ?, ?)" UpdateCommand="UPDATE [Software] SET [Name] = ?, [Version] = ?, [ReleaseDate] = ? WHERE [SoftwareID] = ?" OnInserted="sqldsInsertSoftware_Inserted">
                    <DeleteParameters>
                        <asp:Parameter Name="SoftwareID" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="SoftwareID" Type="String" />
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Version" Type="Decimal" />
                        <asp:Parameter Name="ReleaseDate" Type="DateTime" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Version" Type="Decimal" />
                        <asp:Parameter Name="ReleaseDate" Type="DateTime" />
                        <asp:Parameter Name="SoftwareID" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </section>
        </section>
</asp:Content>

