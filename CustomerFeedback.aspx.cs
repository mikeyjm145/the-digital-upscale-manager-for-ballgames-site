﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
///     The Customer Feedback Code Behind
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public partial class CustomerFeedback : Page
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.tbCustomerId.Focus();
    }

    /// <summary>
    ///     Handles the Click event of the btnCustomerIDCheck control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnCustomerIDCheck_Click(object sender, EventArgs e)
    {
        this.tbCustomerId.Focus();
        this.EnableControls(false);
        this.lbCustomerFeedback.Items.Clear();

        this.FireValidators(this.tbCustomerId.Text.Length == 0, !Regex.IsMatch(this.tbCustomerId.Text, @"^[0-9]+$"));

        this.UpdateCustomerFeebackBox();
    }

    /// <summary>
    ///     Handles the TextChanged event of the tbCustomerId control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void tbCustomerId_TextChanged(object sender, EventArgs e)
    {
        this.btnCustomerIDCheck_Click(sender, e);
    }

    /// <summary>
    ///     Handles the Click event of the btnSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        var description = this.CreateDescription();
        Session["Contact"] = description.Contact;
        Server.Transfer("~/FeedbackComplete.aspx");
    }

    /// <summary>
    ///     Creates the description.
    /// </summary>
    /// <returns>The description object</returns>
    private Description CreateDescription()
    {
        var description = new Description
        {
            CustomerId = Convert.ToInt32(this.tbCustomerId.Text),
            FeedbackId = Convert.ToInt32(this.lbCustomerFeedback.Items[this.lbCustomerFeedback.SelectedIndex].Value),
            Resolution =
                Convert.ToInt32(
                    this.rbgSatisfactoryRatingProblemResolution.Items[
                        this.rbgSatisfactoryRatingProblemResolution.SelectedIndex].Value),
            ServiceTime =
                Convert.ToInt32(
                    this.rbgSatisfactoryRatingServiceTime.Items[this.rbgSatisfactoryRatingServiceTime.SelectedIndex]
                        .Value),
            Comments = this.tbAdditionalComments.Text,
            ContactMethod = this.rbgContactBy.SelectedValue,
            Contact = this.cbContact.Checked
        };

        return description;
    }

    /// <summary>
    ///     Fires the validators.
    /// </summary>
    /// <param name="isEmpty">if set to <c>true</c> [is empty].</param>
    /// <param name="isNotValid">if set to <c>true</c> [is not valid].</param>
    private void FireValidators(bool isEmpty, bool isNotValid)
    {
        if (isEmpty)
        {
            this.rfvCustomerID.IsValid = false;
        }
        else if (isNotValid)
        {
            this.cvCustomerID.IsValid = false;
        }
        else
        {
            this.cvCustomerID.IsValid = true;
            this.rfvCustomerID.IsValid = true;
        }
    }

    /// <summary>
    ///     Updates the customer feeback box.
    /// </summary>
    private void UpdateCustomerFeebackBox()
    {
        this.lbCustomerFeedback.Items.Clear();

        var customerFeedbackListItems = this.GetCustomerFeedback();

        if (customerFeedbackListItems == null)
        {
            this.lbCustomerFeedback.Items.Add(new ListItem("No Customer feedbacks found.", "null"));
            this.EnableControls(false);
            return;
        }

        this.lbCustomerFeedback.Items.AddRange(customerFeedbackListItems);
        this.EnableControls(true);
        this.lbCustomerFeedback.Focus();
    }

    /// <summary>
    ///     Gets the customer feedback.
    /// </summary>
    /// <returns>List of formatted customer information</returns>
    private ListItem[] GetCustomerFeedback()
    {
        var feedbackTable = (DataView) this.sdsCustomerFeedbackSource.Select(DataSourceSelectArguments.Empty);

        if (feedbackTable == null || !Regex.IsMatch(this.tbCustomerId.Text, @"^[0-9]+$"))
        {
            return null;
        }

        feedbackTable.RowFilter = string.Format("CustomerID = '{0}' AND DateClosed IS NOT NULL",
            Convert.ToInt32(this.tbCustomerId.Text));

        return feedbackTable.Count == 0 ? null : this.CreateListItems(feedbackTable);
    }

    /// <summary>
    ///     Creates the list items.
    /// </summary>
    /// <param name="feedbackTable">The feedback table.</param>
    /// <returns>List of formatted customer information </returns>
    private ListItem[] CreateListItems(IEnumerable feedbackTable)
    {
        var customerFeedbackListItems = new List<ListItem>();
        var tempFeedbackItems = new List<Feedback>();

        foreach (DataRowView row in feedbackTable)
        {
            var tempFeedback = new Feedback
            {
                CustomerId = row["CustomerID"].ToString(),
                DateClosed = row["DateClosed"].ToString(),
                DateOpen = row["DateOpened"].ToString(),
                Description = row["Description"].ToString(),
                FeedbackId = row["FeedbackID"].ToString(),
                SoftwareId = row["SoftwareID"].ToString(),
                SupportId = row["SupportID"].ToString(),
                Title = row["Title"].ToString()
            };

            tempFeedbackItems.Add(tempFeedback);
        }

        foreach (var feedback in tempFeedbackItems.ToArray().OrderBy(date => date.DateClosed))
        {
            customerFeedbackListItems.Add(new ListItem(feedback.FormatFeedback(), feedback.FeedbackId));
        }

        return customerFeedbackListItems.ToArray();
    }

    /// <summary>
    ///     Enables the controls.
    /// </summary>
    /// <param name="enable">if set to <c>true</c> [enable].</param>
    private void EnableControls(bool enable)
    {
        this.lbCustomerFeedback.Enabled = enable;

        this.rbgSatisfactoryRatingProblemResolution.Enabled = enable;
        if (this.rbgSatisfactoryRatingProblemResolution.SelectedItem != null)
        {
            this.rbgSatisfactoryRatingProblemResolution.SelectedItem.Selected = false;
        }

        this.rbgSatisfactoryRatingServiceTime.Enabled = enable;
        if (this.rbgSatisfactoryRatingServiceTime.SelectedItem != null)
        {
            this.rbgSatisfactoryRatingServiceTime.SelectedItem.Selected = false;
        }

        this.rbgSatisfactoryRatingTechnicalEfficiency.Enabled = enable;
        if (this.rbgSatisfactoryRatingTechnicalEfficiency.SelectedItem != null)
        {
            this.rbgSatisfactoryRatingTechnicalEfficiency.SelectedItem.Selected = false;
        }

        this.tbAdditionalComments.Enabled = enable;
        this.tbAdditionalComments.Text = "";

        this.rbgContactBy.Enabled = enable;
        if (this.rbgContactBy.SelectedItem != null)
        {
            this.rbgContactBy.SelectedItem.Selected = false;
        }

        this.cbContact.Enabled = enable;
        this.cbContact.Checked = false;
        this.btnSubmit.Enabled = enable;
    }
}