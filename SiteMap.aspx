﻿<%@ Page Title="Site Map" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="SiteMap.aspx.cs" Inherits="SiteMap" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
    <section id="customer_selection_area">
        <section id="centerSiteMap">
            <asp:TreeView ID="tvSiteMap" runat="server" DataSourceID="DUMBSiteMap" Font-Bold="True" Font-Italic="False" Font-Size="1em" ForeColor="AntiqueWhite" ImageSet="Arrows" NodeIndent="4" NodeWrap="True" ShowLines="True" Width="90%">
                <HoverNodeStyle Font-Underline="True" ForeColor="White" />
                <LeafNodeStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="AntiqueWhite" />
                <NodeStyle Font-Names="Tahoma" ForeColor="AntiqueWhite" />
                <ParentNodeStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Underline="False" ForeColor="AntiqueWhite" />
                <RootNodeStyle Font-Bold="True" ForeColor="AntiqueWhite" />
                <SelectedNodeStyle Font-Bold="True" Font-Italic="True" Font-Underline="True" ForeColor="AntiqueWhite" HorizontalPadding="0px" VerticalPadding="0px" />
            </asp:TreeView>
            <asp:SiteMapDataSource ID="DUMBSiteMap" runat="server" />
        </section>
    </section>
</asp:Content>