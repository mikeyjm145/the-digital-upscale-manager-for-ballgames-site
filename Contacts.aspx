﻿<%@ Page Title="DUMB Customer Database" MasterPageFile="~/DUMBMasterPage.master" Language="C#" AutoEventWireup="true" CodeFile="Contacts.aspx.cs" Inherits="Contacts" %>

<asp:Content id="PageContent" runat="server" ContentPlaceHolderID="Content">
        <section id="customer_selection_area">
            <section class="button_section">
                <label class="view_customer_labels">Select a Customer: </label>

                <asp:DropDownList ID="ddlCustomers" CssClass="ddlCustomersView" runat="server" DataSourceID="CustomerDatabase" AutoPostBack="True" DataTextField="Name" DataValueField="CustomerID" OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged" TabIndex="1">
                </asp:DropDownList>
                <asp:SqlDataSource ID="CustomerDatabase" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [CustomerID], [Name], [City], [Address], [State], [ZipCode], [Phone], [Email] FROM [Customer] ORDER BY [Name]"></asp:SqlDataSource>
            </section>

            <section class="button_section">
                <asp:PlaceHolder ID="phErrorMessage" runat="server" Visible="False">
                    <asp:Label ID="lblErrorMessage" runat="server" CssClass="errorMessage"></asp:Label>
                </asp:PlaceHolder>
            </section>

            <section class="button_section">
                &nbsp;&nbsp; <asp:Button ID="btnViewContactsList" runat="server" Text="View Contacts List" CssClass="form_buttons" PostBackUrl="~/ContactsList.aspx" TabIndex="2"/>
                &nbsp;&nbsp; <asp:Button ID="btnAddToContacts" runat="server" Text="Add To Contacts" CssClass="form_buttons" OnClick="btnAddToContacts_Click" TabIndex="3"/>
            </section>
        </section>

        <section id="customer_information">
            <asp:Label ID="lblCustomerName" CssClass="lblCustomerInfo" runat="server" Text="Change" OnDataBinding="ddlCustomers_SelectedIndexChanged"></asp:Label>

            <br/>
            <br/>

            <label class="view_customer_sublabels">Phone:</label>
            <asp:Label ID="lblPhoneNumber" runat="server" CssClass="lblCustomerInfo" Text="Change"></asp:Label>

            <br/>

            <label class="view_customer_sublabels">Email:</label>
            <asp:Label ID="lblEmail" runat="server" Text="Change" CssClass="lblCustomerInfo"></asp:Label>
        </section>
</asp:Content>