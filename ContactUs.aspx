﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
    <section id="contact_us_text" class="button_section">
        <section>
            <span class="bold pageTitle">Hours of Operation:</span>
            <section class="indent">Monday-Friday 9am - 5pm</section>

            <br/>

            <span class="bold pageTitle">Contact us through</span>
            <section class="indent">
                <span class="bold">Phone:</span>
                <span class="italic">88-BallGame</span> or 
                <span class="italic">(882) 255 - 4263</span>
                <br/>
                <span class="bold">Email</span>
                <asp:HyperLink ID="HyperLink1" NavigateUrl="#" runat="server">
                    info@ballgame.com
                </asp:HyperLink>
                <br/>
                <span class="bold">Mail To:</span>
                123 ASPNET Road, Carrollton, Georgia 30127-103
            </section>
        </section>
    </section>
</asp:Content>

