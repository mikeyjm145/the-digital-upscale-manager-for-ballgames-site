﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for the Customer Maintenance page
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class CustomerMaintenance2 : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the Inserted event of the sqldsCustomerMaintenanceAddInsertDelete control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.SqlDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void sqldsCustomerMaintenanceAddInsertDelete_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        const string exception = "The changes you requested to the table were not successful because " +
                                 "they would create duplicate values in the index, primary key, or " +
                                 "relationship. Change the data in the field or fields that contain " +
                                 "duplicate data, remove the index, or redefine the index to permit " +
                                 "duplicate entries and try again.";

        if (e.Exception == null || e.Exception.Message.Equals(exception))
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.gvCustomerMaintenance.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the ItemInserted event of the dvCustomerMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewInsertedEventArgs"/> instance containing the event data.</param>
    protected void dvCustomerMaintenance_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        const string exception = "The changes you requested to the table were not successful because " +
                                 "they would create duplicate values in the index, primary key, or " +
                                 "relationship. Change the data in the field or fields that contain " +
                                 "duplicate data, remove the index, or redefine the index to permit " +
                                 "duplicate entries and try again.";
        if (e.Exception == null || e.Exception.Message.Equals(exception))
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.gvCustomerMaintenance.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.KeepInInsertMode = true;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the Deleted event of the sqldsCustomerMaintenanceAddInsertDelete control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.SqlDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void sqldsCustomerMaintenanceAddInsertDelete_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.gvCustomerMaintenance.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the ItemDeleted event of the dvCustomerMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewDeletedEventArgs"/> instance containing the event data.</param>
    protected void dvCustomerMaintenance_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
                return;
            }

            this.gvCustomerMaintenance.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the Updated event of the sqldsCustomerMaintenanceAddInsertDelete control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.SqlDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void sqldsCustomerMaintenanceAddInsertDelete_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected.";
            }

            this.gvCustomerMaintenance.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the ItemUpdated event of the dvCustomerMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void dvCustomerMaintenance_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            if (e.AffectedRows == 0)
            {
                this.lblErrorMessage.Text = "No rows were affected";
            }

            this.gvCustomerMaintenance.DataBind();
            return;
        }

        this.lblErrorMessage.Text = e.Exception.Message;
        e.ExceptionHandled = true;
        e.KeepInEditMode = true;
    }
}