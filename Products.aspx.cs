﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for Products page
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class Products : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    /// <summary>
    /// Handles the ServerValidate event of the cvSoftwareVersion control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="args">The <see cref="ServerValidateEventArgs"/> instance containing the event data.</param>
    protected void cvSoftwareVersion_ServerValidate(object source, ServerValidateEventArgs args)
    {
        var index = this.gvSoftwareProducts.EditIndex;
        var version = (TextBox)this.gvSoftwareProducts.Rows[index].FindControl("tbSoftwareVersion");
        
        decimal value;
        if (Decimal.TryParse(version.Text, out value))
        {
            args.IsValid = true;
            return;
        }

        args.IsValid = false;
    }

    /// <summary>
    /// Handles the Inserted event of the sqldsInsertSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="SqlDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void sqldsInsertSoftware_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.Exception == null)
        {
            this.gvSoftwareProducts.DataBind();
            return;
        }

        var message = e.Exception.Message;
        Console.WriteLine(message);
        e.ExceptionHandled = true;
    }

    /// <summary>
    /// Handles the RowUpdated event of the gvSoftwareProducts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvSoftwareProducts_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblErrorMessage.Text = "";
            return;
        }

        this.lblErrorMessage.Text = "A database method has occurred.<br/><br/>" +
                             "Message: " + e.Exception.Message;
        e.ExceptionHandled = true;
        e.KeepInEditMode = true;
    }

    /// <summary>
    /// Handles the ItemInserted event of the fvInsertSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="FormViewInsertedEventArgs"/> instance containing the event data.</param>
    protected void fvInsertSoftware_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.gvSoftwareProducts.DataBind();
            return;
        }

        var message = e.Exception.Message;
        Console.WriteLine(message);
        e.ExceptionHandled = true;
    }
}