﻿<%@ Page Title="DUMB Customer Database" MasterPageFile="~/DUMBMasterPage.master" Language="C#" AutoEventWireup="true" CodeFile="ContactsList.aspx.cs" Inherits="ContactsList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" Runat="Server">
    <section id="customer_selection_area">
            <label class="view_customer_labels title_spacing">Customer List</label>

            <br/>

            <asp:ListBox ID="lbCustomerListView" CssClass="lbCustomerListView" runat="server" ToolTip="List of Customers" TabIndex="1">
            </asp:ListBox>

            <br/>
            <br/>

            <section class="button_section">
                <asp:Button ID="btnSelectAdditionalCustomers" runat="server" CssClass="form_buttons" Text="Select Additional Customers" CausesValidation="False" TabIndex="2" UseSubmitBehavior="False" PostBackUrl="~/Contacts.aspx"/>
                &nbsp;
                <asp:Button ID="btnRemoveCustomer" runat="server" CssClass="form_buttons" Text="Remove Customer" OnClick="btnRemoveCustomer_Click" ToolTip="Removes a customer from the list" CausesValidation="False" EnableViewState="False" TabIndex="3" UseSubmitBehavior="False" ValidateRequestMode="Disabled" ViewStateMode="Disabled"/>
                &nbsp;
                <asp:Button ID="btnClearList" runat="server" CausesValidation="False" CssClass="form_buttons" OnClick="btnClearList_Click" Text="Clear List" ToolTip="Clears the list of customers" TabIndex="4" UseSubmitBehavior="False"/>
            </section>
        </section>
</asp:Content>