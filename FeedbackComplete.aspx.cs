﻿using System;
using System.Web.UI;

/// <summary>
///     The Feedback Complete Code Behind
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public partial class FeedbackComplete : Page
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Contact"] == null)
        {
            this.SetLabelValues("Invalid Page Access", "Please select a link below to browse another page.");
            return;
        }

        this.ShowContactMessage((bool) Session["Contact"]);
    }

    /// <summary>
    ///     Shows the contact message.
    /// </summary>
    /// <param name="contact">if set to <c>true</c> [contact].</param>
    private void ShowContactMessage(bool contact)
    {
        if (contact)
        {
            this.SetLabelValues("Feedback Recieved",
                "Your feedback was successfully recieved. We will contact you within 24 hours." +
                "<br/><span class='changeColors'>Please navigate to one of the links below.</span>");
        }
        else
        {
            this.SetLabelValues("Feedback Recieved",
                "Your feedback was successfully recieved." +
                "<br/><span class='changeColors'>Please navigate to one of the links below.</span>");
        }
    }

    /// <summary>
    ///     Sets the label values.
    /// </summary>
    /// <param name="title">The title.</param>
    /// <param name="text">The text.</param>
    private void SetLabelValues(string title, string text)
    {
        this.lblConfirmationTitle.Text = title;
        this.lblConfirmationText.Text = text;
    }
}