﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;

/// <summary>
/// Summary description for SupportDB
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
[DataObject(true)]
public class SupportDb
{
    /// <summary>
    /// Gets the support staff members.
    /// </summary>
    /// <returns>List of Support Staff</returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable<SupportStaffMember> GetSupportStaffMembers()
    {
        var oleDbConnection = new OleDbConnection(BallgameDb.GetSupportConnectionString());
        const string oleDbSelectStatement = "SELECT * FROM [Support] ORDER BY [Name]";

        var oleDbCommand = new OleDbCommand(oleDbSelectStatement, oleDbConnection);

        oleDbConnection.Open();
        var oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);

        if (oleDbDataReader == null)
        {
            throw new InvalidDataException("Could not execute query.");
        }

        var staffMemberList = GenerateMainStaffMembersList(oleDbDataReader);
        oleDbDataReader.Close();

        return staffMemberList;
    }

    private static IEnumerable<SupportStaffMember> GenerateMainStaffMembersList(IDataReader oleDbDataReader)
    {
        var staffMembers = new List<SupportStaffMember>();
        while (oleDbDataReader.Read())
        {
            var currStaffMember = new SupportStaffMember
            {
                SupportId = Convert.ToInt32(oleDbDataReader["SupportID"].ToString()),
                Name = oleDbDataReader["Name"].ToString(),
                Email = oleDbDataReader["Email"].ToString(),
                Phone = oleDbDataReader["Phone"].ToString()
            };

            staffMembers.Add(currStaffMember);
        }

        return staffMembers;
    }
}