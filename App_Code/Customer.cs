﻿using System.Diagnostics;

/// <summary>
///     Summary description for Customer
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public class Customer
{
    /// <summary>
    ///     Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    ///     The customer identifier.
    /// </value>
    public string CustomerId
    {
        get { return this._customerId; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the customer ID.");
            this._customerId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the name.
    /// </summary>
    /// <value>
    ///     The name.
    /// </value>
    public string Name
    {
        get { return this._name; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the name.");
            this._name = value;
        }
    }

    /// <summary>
    ///     Gets or sets the address.
    /// </summary>
    /// <value>
    ///     The address.
    /// </value>
    public string Address
    {
        get { return this._address; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the address.");
            this._address = value;
        }
    }

    /// <summary>
    ///     Gets or sets the city.
    /// </summary>
    /// <value>
    ///     The city.
    /// </value>
    public string City
    {
        get { return this._city; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the city.");
            this._city = value;
        }
    }

    /// <summary>
    ///     Gets or sets the state.
    /// </summary>
    /// <value>
    ///     The state.
    /// </value>
    public string State
    {
        get { return this._state; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the state.");
            this._state = value;
        }
    }

    /// <summary>
    ///     Gets or sets the zip code.
    /// </summary>
    /// <value>
    ///     The zip code.
    /// </value>
    public string ZipCode
    {
        get { return this._zipCode; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the zip code.");
            this._zipCode = value;
        }
    }

    /// <summary>
    ///     Gets or sets the phone number.
    /// </summary>
    /// <value>
    ///     The phone number.
    /// </value>
    public string PhoneNumber
    {
        get { return this._phoneNumber; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the phone number");
            this._phoneNumber = value;
        }
    }

    /// <summary>
    ///     Gets or sets the email.
    /// </summary>
    /// <value>
    ///     The email.
    /// </value>
    public string Email
    {
        get { return this._email; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the email.");
            this._email = value;
        }
    }

    private string _address;
    private string _city;
    private string _customerId;
    private string _email;
    private string _name;
    private string _phoneNumber;
    private string _state;
    private string _zipCode;

    /// <summary>
    ///     Initializes a new instance of the <see cref="Customer" /> class.
    /// </summary>
    public Customer()
    {
        this._customerId = "";
        this._name = "";
        this._address = "";
        this._city = "";
        this._state = "";
        this._zipCode = "-1";
        this._phoneNumber = "";
        this._email = "";
    }
}