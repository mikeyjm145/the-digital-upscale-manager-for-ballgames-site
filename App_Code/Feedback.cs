﻿using System.Diagnostics;

/// <summary>
///     Summary description for Feedback
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public class Feedback
{
    /// <summary>
    ///     Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    ///     The customer identifier.
    /// </value>
    public string CustomerId
    {
        get { return this._customerId; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the customer ID.");
            this._customerId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the feedback identifier.
    /// </summary>
    /// <value>
    ///     The feedback identifier.
    /// </value>
    public string FeedbackId
    {
        get { return this._feedbackId; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the feedback ID.");
            this._feedbackId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the software identifier.
    /// </summary>
    /// <value>
    ///     The software identifier.
    /// </value>
    public string SoftwareId
    {
        get { return this._softwareId; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the software ID.");
            this._softwareId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the support identifier.
    /// </summary>
    /// <value>
    ///     The support identifier.
    /// </value>
    public string SupportId
    {
        get { return this._supportId; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the support ID.");
            this._supportId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the date open.
    /// </summary>
    /// <value>
    ///     The date open.
    /// </value>
    public string DateOpen
    {
        get { return this._dateOpen; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the Date Open.");
            this._dateOpen = value;
        }
    }

    /// <summary>
    ///     Gets or sets the date closed.
    /// </summary>
    /// <value>
    ///     The date closed.
    /// </value>
    public string DateClosed
    {
        get { return this._dateClosed; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the Date Closed.");
            this._dateClosed = value;
        }
    }

    /// <summary>
    ///     Gets or sets the title.
    /// </summary>
    /// <value>
    ///     The title.
    /// </value>
    public string Title
    {
        get { return this._title; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the Title.");
            this._title = value;
        }
    }

    /// <summary>
    ///     Gets or sets the description.
    /// </summary>
    /// <value>
    ///     The description.
    /// </value>
    public string Description
    {
        get { return this._description; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the Description.");
            this._description = value;
        }
    }

    private string _customerId;
    private string _dateClosed;
    private string _dateOpen;
    private string _description;
    private string _feedbackId;
    private string _softwareId;
    private string _supportId;
    private string _title;

    /// <summary>
    ///     Initializes a new instance of the <see cref="Feedback" /> class.
    /// </summary>
    public Feedback()
    {
        this._customerId = "";
        this._feedbackId = "";
        this._supportId = "";
        this._softwareId = "";

        this._dateOpen = "";
        this._dateClosed = "";
        this._description = "";
        this._title = "";
    }

    /// <summary>
    ///     Formats the feedback.
    /// </summary>
    /// <returns>The formatted message displaying when it has been closed</returns>
    public string FormatFeedback()
    {
        return "Feedback for "
               + this.SoftwareId
               + " closed " + this.DateClosed
               + " (" + this.Title + ")";
    }
}