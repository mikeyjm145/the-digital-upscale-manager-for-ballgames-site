﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Web;

/// <summary>
///     Summary description for CustomerList
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public class CustomerList
{
    /// <summary>
    ///     Gets or sets the <see cref="Customer" /> at the specified index.
    /// </summary>
    /// <value>
    ///     The <see cref="Customer" />.
    /// </value>
    /// <param name="index">The index.</param>
    /// <returns>The customer at this index</returns>
    public Customer this[int index]
    {
        get { return this._customerList[index]; }
        set
        {
            Trace.Assert(value != null, "Customer does not exist at this index.");
            this._customerList.Insert(index, value);
        }
    }

    /// <summary>
    ///     Gets the <see cref="Customer" /> with the specified name.
    /// </summary>
    /// <value>
    ///     The <see cref="Customer" />.
    /// </value>
    /// <param name="name">The name.</param>
    /// <returns>The customer in the list</returns>
    public Customer this[string name]
    {
        get { return this._customerList.Find(customer => customer.Name.Equals(name)); }
    }

    /// <summary>
    ///     Gets the count.
    /// </summary>
    /// <value>
    ///     The count.
    /// </value>
    public int Count
    {
        get { return this._customerList.Count; }
    }

    private readonly List<Customer> _customerList;

    /// <summary>
    ///     Initializes a new instance of the <see cref="CustomerList" /> class.
    /// </summary>
    public CustomerList()
    {
        this._customerList = new List<Customer>();
    }

    /// <summary>
    ///     Adds the specified a new customer.
    /// </summary>
    /// <param name="aNewCustomer">a new customer.</param>
    public void Add(Customer aNewCustomer)
    {
        Trace.Assert(aNewCustomer != null, "The customer does not exist.");
        this._customerList.Add(aNewCustomer);
    }

    /// <summary>
    ///     Removes at.
    /// </summary>
    /// <param name="index">The index.</param>
    public void RemoveAt(int index)
    {
        Trace.Assert(index < this.Count, "The index is out of bounds.");
        this._customerList.RemoveAt(index);
    }

    /// <summary>
    ///     Clears this instance.
    /// </summary>
    public void Clear()
    {
        this._customerList.Clear();
    }

    /// <summary>
    ///     Gets the customers.
    /// </summary>
    /// <returns>The customer list from the previous session</returns>
    public static CustomerList GetCustomers()
    {
        var customerList = (CustomerList) HttpContext.Current.Session["Customers"];

        if (customerList == null)
        {
            HttpContext.Current.Session["Customers"] = new CustomerList();
        }

        return (CustomerList) HttpContext.Current.Session["Customers"];
    }
}