﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;

/// <summary>
/// Summary description for CustomerDB
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
[DataObject(true)]
public class CustomerDb
{
    /// <summary>
    /// Gets the support staff members.
    /// </summary>
    /// <returns>List of Support Staff</returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable<Customer> GetSupportStaffMembers()
    {
        var oleDbConnection = new OleDbConnection(BallgameDb.GetCustomerInformationConnectionString());
        const string oleDbSelectStatement = "SELECT CustomerID, Name" +
                                            " FROM Customer" +
                                            " WHERE (CustomerID" +
                                            " IN (SELECT DISTINCT CustomerID FROM Feedback" +
                                            " WHERE (SupportID IS NOT NULL)))" +
                                            " ORDER BY Name";

        var oleDbCommand = new OleDbCommand(oleDbSelectStatement, oleDbConnection);

        oleDbConnection.Open();
        var oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);

        if (oleDbDataReader == null)
        {
            throw new InvalidDataException("Could not execute query.");
        }

        var staffMemberList = GenerateCustomerList(oleDbDataReader);
        oleDbDataReader.Close();

        return staffMemberList;
    }

    private static IEnumerable<Customer> GenerateCustomerList(IDataReader oleDbDataReader)
    {
        var customers = new List<Customer>();
        while (oleDbDataReader.Read())
        {
            var currCustomer = new Customer
            {
                CustomerId = oleDbDataReader["CustomerID"].ToString(),
                Name = oleDbDataReader["Name"].ToString()
            };

            customers.Add(currCustomer);
        }

        return customers;
    }
}