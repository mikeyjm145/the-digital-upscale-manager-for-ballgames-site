﻿using System.Configuration;

/// <summary>
/// Summary description for BallgameDb
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public class BallgameDb
{
    /// <summary>
    /// Gets the connection string for Support Personnel Connection String.
    /// </summary>
    /// <returns>The product database connection string</returns>
    public static string GetSupportConnectionString()
    {
        return ConfigurationManager
            .ConnectionStrings["SupportPersonnelConnectionString"].ConnectionString;
    }

    /// <summary>
    /// Gets the Customer Incidents connection string.
    /// </summary>
    /// <returns></returns>
    public static string GetIncidentsConnectionString()
    {
        return ConfigurationManager
            .ConnectionStrings["CustomerIncidentsConnectionString"].ConnectionString;
    }

    /// <summary>
    /// Gets the feedback information connection string.
    /// </summary>
    /// <returns></returns>
    public static string GetFeedbackInformationConnectionString()
    {
        return ConfigurationManager
            .ConnectionStrings["FeedbackInformationConnectionString"].ConnectionString;
    }

    /// <summary>
    /// Gets the customer information connection string.
    /// </summary>
    /// <returns></returns>
    public static string GetCustomerInformationConnectionString()
    {
        return ConfigurationManager
            .ConnectionStrings["CustomerInformationConnectionString"].ConnectionString;
    }
}