﻿using System;

/// <summary>
/// Summary description for OpenFeedback
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public class OpenFeedback
{
    private string _softwareName;
    private string _customerName;
    private string _dateOpened;

    /// <summary>
    /// Gets or sets the software.
    /// </summary>
    /// <value>
    /// The software.
    /// </value>
    /// <exception cref="System.Exception">Software name cannot be null.</exception>
    public string Software
    {
        get { return this._softwareName; }
        set {
            if (value == null)
            {
                throw new Exception("Software name cannot be null.");
                
            }
            
            this._softwareName = value;
        }
    }

    /// <summary>
    /// Gets or sets the customer.
    /// </summary>
    /// <value>
    /// The customer.
    /// </value>
    /// <exception cref="System.Exception">Customer name cannot be null.</exception>
    public string Customer
    {
        get { return this._customerName; }
        set
        {
            if (value == null)
            {
                throw new Exception("Customer name cannot be null.");

            }

            this._customerName = value;
        }
    }

    /// <summary>
    /// Gets or sets the date opened.
    /// </summary>
    /// <value>
    /// The date opened.
    /// </value>
    /// <exception cref="System.Exception">Date Opened cannot be null.</exception>
    public string DateOpened
    {
        get { return this._dateOpened; }
        set
        {
            if (value == null)
            {
                throw new Exception("Date Opened cannot be null.");

            }

            this._dateOpened = value;
        }
    }
}