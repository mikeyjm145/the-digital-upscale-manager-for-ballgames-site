﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;

/// <summary>
/// Summary description for FeedbackDB
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
[DataObject(true)]
public class FeedbackDb
{
    /// <summary>
    /// Gets the open feedback incidents.
    /// </summary>
    /// <param name="supportStaffId">The support staff identifier.</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException">Could not execute query.</exception>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable<OpenFeedback> GetOpenFeedbackIncidents(int supportStaffId)
    {
        var oleDbConnection = new OleDbConnection(BallgameDb.GetIncidentsConnectionString());
        var oleDbSelectStatement = "SELECT Software.Name, Support.Name AS aName, Customer.Name AS bName, Feedback.DateOpened, Feedback.DateClosed, Feedback.Description" +
                                   " FROM (((Feedback INNER JOIN Software ON Feedback.SoftwareID = Software.SoftwareID)" +
                                   " INNER JOIN Support ON Feedback.SupportID = Support.SupportID)" +
                                   " INNER JOIN Customer ON Feedback.CustomerID = Customer.CustomerID)" +
                                   " WHERE (Feedback.SupportID = " + supportStaffId + ") AND (Feedback.DateClosed IS NULL)" +
                                   " ORDER BY Feedback.DateOpened";

        var oleDbCommand = new OleDbCommand(oleDbSelectStatement, oleDbConnection);

        oleDbConnection.Open();
        var oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);

        if (oleDbDataReader == null)
        {
            throw new InvalidDataException("Could not execute query.");
        }

        var incidentsList = GenerateIncidentsList(oleDbDataReader);
        oleDbDataReader.Close();

        return incidentsList;
    }

    private static IEnumerable<OpenFeedback> GenerateIncidentsList(IDataReader oleDbDataReader)
    {
        var feedback = new List<OpenFeedback>();
        while (oleDbDataReader.Read())
        {
            var currFeedback = new OpenFeedback
            {
                DateOpened = oleDbDataReader["DateOpened"].ToString(),
                Software = oleDbDataReader["Name"].ToString(),
                Customer = oleDbDataReader["bName"].ToString()
            };

            feedback.Add(currFeedback);
        }

        return feedback;
    }

    /// <summary>
    /// Gets the customer feedback.
    /// </summary>
    /// <param name="customerId">The customer identifier.</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException">Could not execute query.</exception>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static IEnumerable<Feedback> GetCustomerFeedback(int customerId)
    {
        var oleDbConnection = new OleDbConnection(BallgameDb.GetFeedbackInformationConnectionString());
        var oleDbSelectStatement = "SELECT [FeedbackID], [CustomerID], [DateOpened], [DateClosed], [Title], [Description], [SupportID], [SoftwareID]" +
                                   " FROM [Feedback]" +
                                   " WHERE ([CustomerID] = " + customerId +")";

        var oleDbCommand = new OleDbCommand(oleDbSelectStatement, oleDbConnection);

        oleDbConnection.Open();
        var oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);

        if (oleDbDataReader == null)
        {
            throw new InvalidDataException("Could not execute query.");
        }

        var feedbackList = GenerateFeedbackList(oleDbDataReader);
        oleDbDataReader.Close();

        return feedbackList;
    }

    private static IEnumerable<Feedback> GenerateFeedbackList(IDataReader oleDbDataReader)
    {
        var feedback = new List<Feedback>();
        while (oleDbDataReader.Read())
        {
            var currFeedback = new Feedback
            {
                FeedbackId = oleDbDataReader["FeedbackID"].ToString(),
                SupportId = oleDbDataReader["SupportID"].ToString(),
                SoftwareId = oleDbDataReader["SoftwareID"].ToString(),
                DateOpen = oleDbDataReader["DateOpened"].ToString(),
                DateClosed = oleDbDataReader["DateClosed"].ToString(),
                Title = oleDbDataReader["Title"].ToString(),
                Description = oleDbDataReader["Description"].ToString()
            };

            feedback.Add(currFeedback);
        }

        return feedback;
    }

    /// <summary>
    /// Updates the feedback.
    /// </summary>
    /// <param name="originalFeedbackId">The feedback identifier.</param>
    /// <param name="dateClosed">The date closed.</param>
    /// <param name="description">The description.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int UpdateFeedback(
        string originalFeedbackId,
        string dateClosed,
        string description)
    {
        var oleDbConnection = new OleDbConnection(BallgameDb.GetFeedbackInformationConnectionString());

        var oleDbUpdateStatement =
            "UPDATE [Feedback] " +
            "SET [DateClosed] = @DateClosed," +
            " [Description] = '" + description + "'" +
            " WHERE [FeedbackID] = " + originalFeedbackId;
        
        var command = new OleDbCommand(oleDbUpdateStatement, oleDbConnection);

        if (Convert.ToDateTime(dateClosed) == Convert.ToDateTime("01/01/0001 12:00:00 AM"))
        {
            command.Parameters.AddWithValue("DateClosed", DBNull.Value);
        }
        else
        {
            command.Parameters.AddWithValue("DateClosed", dateClosed);
        }

        oleDbConnection.Open();
        var updateCount = command.ExecuteNonQuery();
        oleDbConnection.Close();

        return updateCount;
    }
}