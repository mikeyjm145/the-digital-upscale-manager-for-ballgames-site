﻿using System.Diagnostics;

/// <summary>
///     Summary description for Description
/// </summary>
/// <author>
///     Michael Morguarge
/// </author>
/// <version>
///     1.0
/// </version>
public class Description
{
    /// <summary>
    ///     Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    ///     The customer identifier.
    /// </value>
    public int CustomerId
    {
        get { return this._customerId; }
        set
        {
            Trace.Assert(value > 0, "Invalid value for the customer ID.");
            this._customerId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the feedback identifier.
    /// </summary>
    /// <value>
    ///     The feedback identifier.
    /// </value>
    public int FeedbackId
    {
        get { return this._feedbackId; }
        set
        {
            Trace.Assert(value > 0, "Invalid value for the feedback ID.");
            this._feedbackId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the service time.
    /// </summary>
    /// <value>
    ///     The service time.
    /// </value>
    public int ServiceTime
    {
        get { return this._serviceTime; }
        set
        {
            Trace.Assert(value > 0, "Invalid value for the Service Time.");
            this._serviceTime = value;
        }
    }

    /// <summary>
    ///     Gets or sets the resolution.
    /// </summary>
    /// <value>
    ///     The resolution.
    /// </value>
    public int Resolution
    {
        get { return this._resolution; }
        set
        {
            Trace.Assert(value > 0, "Invalid value for the Resolution.");
            this._resolution = value;
        }
    }

    /// <summary>
    ///     Gets or sets the comments.
    /// </summary>
    /// <value>
    ///     The comments.
    /// </value>
    public string Comments
    {
        get { return this._comments; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the Comments.");
            this._comments = value;
        }
    }

    /// <summary>
    ///     Gets or sets the contact method.
    /// </summary>
    /// <value>
    ///     The contact method.
    /// </value>
    public string ContactMethod
    {
        get { return this._contactMethod; }
        set
        {
            Trace.Assert(value != null, "Invalid value for the Contact Method.");
            this._contactMethod = value;
        }
    }

    /// <summary>
    ///     Gets or sets a value indicating whether this <see cref="Description" /> is contact.
    /// </summary>
    /// <value>
    ///     <c>true</c> if contact; otherwise, <c>false</c>.
    /// </value>
    public bool Contact { get; set; }

    private string _comments;
    private string _contactMethod;
    private int _customerId;
    private int _feedbackId;
    private int _resolution;
    private int _serviceTime;

    /// <summary>
    ///     Initializes a new instance of the <see cref="Description" /> class.
    /// </summary>
    public Description()
    {
        this._comments = "";
        this._contactMethod = "";
        this._customerId = 0;
        this._feedbackId = 0;
        this._resolution = 0;
        this._serviceTime = 0;
    }
}