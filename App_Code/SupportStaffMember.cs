﻿using System;

/// <summary>
/// Summary description for SupportStaffMember
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public class SupportStaffMember
{
    private int _supportId;
    private string _name;
    private string _email;
    private string _phone;

    /// <summary>
    /// Gets or sets the support identifier.
    /// </summary>
    /// <value>
    /// The support identifier.
    /// </value>
    /// <exception cref="System.Exception">ID must be greater than 0.</exception>
    public int SupportId
    {
        get { return this._supportId; }
        set
        {
            if (value <= 0)
            {
                throw new Exception("ID must be greater than 0.");
            }

            this._supportId = value;
        }
    }

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    /// <exception cref="System.Exception">Name cannot be null.</exception>
    public string Name
    {
        get { return this._name; }
        set {
            if (value == null)
            {
                throw new Exception("Name cannot be null.");
            }

            this._name = value;
        }
    }

    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    /// <exception cref="System.Exception">Email cannot be null.</exception>
    public string Email
    {
        get { return this._email; }
        set
        {
            if (value == null)
            {
                throw new Exception("Email cannot be null.");
            }

            this._email = value;
        }
    }

    /// <summary>
    /// Gets or sets the phone.
    /// </summary>
    /// <value>
    /// The phone.
    /// </value>
    /// <exception cref="System.Exception">Phone cannot be null.</exception>
    public string Phone
    {
        get { return this._phone; }
        set
        {
            if (value == null)
            {
                throw new Exception("Phone cannot be null.");
            }

            this._phone = value;
        }
    }
}