﻿<%@ Page Title="Home" MasterPageFile="~/DUMBMasterPage.master" Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default"%>

<asp:Content id="PageContent" runat="server" ContentPlaceHolderID="Content">
        <section id="customer_selection_area">
            <label class="view_customer_labels title_spacing">Welcome! Select an option below:</label>

            <br/>

            <section class="button_section">
                <a href="Contacts.aspx" tabindex="1">
                    <section class="home_page_link float_left">
                        <section class="changeColor">
                            <asp:ImageButton ID="ibCustomerView" runat="server" CssClass="ibCustomerView" Height="169px" ImageAlign="AbsMiddle" ImageUrl="~/Images/findpeople.png" PostBackUrl="~/Contacts.aspx" Width="169px"/>
                        </section>
                        <label class="lblCustomerInfo">Customer View</label>
                    </section>
                </a>

                <a href="CustomerFeedback.aspx" tabindex="2">
                    <section class="home_page_link float_right">
                        <section class="changeColor">
                            <asp:ImageButton ID="ibOtherLink" runat="server" Height="169px" ImageAlign="AbsMiddle" ImageUrl="~/Images/feedback.png" PostBackUrl="~/CustomerFeedback.aspx" Width="169px"/>
                        </section>
                        <label class="lblCustomerInfo">Customer Feedback</label>
                    </section>
                </a>
            </section>

            <br/>
        </section>
</asp:Content>