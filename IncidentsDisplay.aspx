﻿<%@ Page Title="Display Incidents" Language="C#" MasterPageFile="~/DUMBMasterPage.master" AutoEventWireup="true" CodeFile="IncidentsDisplay.aspx.cs" Inherits="IncidentsDisplay" %>

<asp:Content ID="PageContent" ContentPlaceHolderID="Content" Runat="Server">
    <section id="customer_selection_area">
        <section class="center border_for_section">
            <label class="center bold pageTitle ddlLabel">Please select a customer: </label>
            <asp:DropDownList ID="ddlCustomersIncidents" CssClass="ddlCustomersView" runat="server" DataSourceID="sqldsCustomers" DataTextField="Name" DataValueField="CustomerID" AutoPostBack="True">
            </asp:DropDownList>
        </section>
        <asp:SqlDataSource ID="sqldsCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerIssuesConnectionString %>" ProviderName="<%$ ConnectionStrings:CustomerIssuesConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT Customer.Name, Customer.CustomerID FROM (Customer INNER JOIN Feedback ON Customer.CustomerID = Feedback.CustomerID) ORDER BY Customer.Name"></asp:SqlDataSource>

        <br/>
        <section class="tableSection center">
            <asp:DataList ID="dlIncidents" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="sqldsIncidents" ForeColor="Black" GridLines="Vertical" HorizontalAlign="Center" Width="781px">
                <AlternatingItemStyle BackColor="White" />
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                <ItemStyle BackColor="#F7F7DE" />
                <HeaderTemplate>
                    <table>
                            <th class="tableCol">Software/Incident</th>
                            <th class="tableCol">Technician Name</th>
                            <th class="tableCol">Date Opened</th>
                            <th class="tableCol">Date Closed</th>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td class="tableColData">
                                <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
                            </td>
                            <td class="tableColData">
                                <asp:Label ID="Expr1Label" runat="server" Text='<%# Eval("Expr1") %>' />
                            </td>
                            <td class="tableColData">
                                <asp:Label ID="DateOpenedLabel" runat="server" Text='<%# Eval("DateOpened") %>' />
                            </td>
                            <td class="tableColData">
                                <asp:Label ID="DateClosedLabel" runat="server" Text='<%# Eval("DateClosed") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td class="alignText" colspan="4">
                                <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>' />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            </asp:DataList>
        </section>
        <asp:SqlDataSource ID="sqldsIncidents" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerIncidentsConnectionString %>" ProviderName="<%$ ConnectionStrings:CustomerIncidentsConnectionString.ProviderName %>" SelectCommand="SELECT Software.Name, Support.Name AS Expr1, Feedback.DateOpened, Feedback.DateClosed, Feedback.Description FROM ((Feedback INNER JOIN Software ON Feedback.SoftwareID = Software.SoftwareID) INNER JOIN Support ON Feedback.SupportID = Support.SupportID) WHERE (Feedback.CustomerID = ?)">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlCustomersIncidents" Name="?" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
    </section>
</asp:Content>

<asp:Content ID="Content1" runat="server" contentplaceholderid="Head">
    <link href="App_Themes/Main.css" rel="stylesheet"/>
    <style type="text/css">
        .auto-style1 {
            width: 125px;
        }
    </style>
</asp:Content>


